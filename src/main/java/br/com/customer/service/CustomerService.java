package br.com.customer.service;

import br.com.customer.exceptions.CustomerNotFoundException;
import br.com.customer.models.Customer;
import br.com.customer.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    public Customer create(Customer customer) {
        return customerRepository.save(customer);
    }

    public Customer getById(Long id) {
        Optional<Customer> byId = customerRepository.findById(id);

        if(!byId.isPresent()) {
            throw new CustomerNotFoundException();
        }

        return byId.get();
    }
}
